from __future__ import unicode_literals

try:
    from south.modelsinspector import add_introspection_rules
    add_introspection_rules([], ["^geoposition_field\.fields\.GeopositionField"])
except ImportError:
    pass
