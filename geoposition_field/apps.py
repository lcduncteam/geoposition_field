from django.apps import AppConfig


class GeoPositionConfig(AppConfig):
    name = 'geoposition_field'
    verbose_name = "GeoPosition Field"
