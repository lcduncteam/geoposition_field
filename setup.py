# coding: utf-8
from setuptools import setup, find_packages

setup(
    name='geoposition-field',
    version=__import__('geoposition_field').__version__,
    description=
    'Django model field that can hold a geoposition, and corresponding admin widget.',
    author='Dario Castañé',
    author_email='i@dario.im',
    url='https://bitbucket.org/lcduncteam/geoposition_field',
    packages=find_packages(),
    zip_safe=False,
    package_data={
        'geoposition_field': [
            'locale/*/LC_MESSAGES/*',
            'templates/geoposition_field/widgets/*.html',
            'static/geoposition_field/*',
        ],
    },
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Framework :: Django',
    ])
